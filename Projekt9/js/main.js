$(document).ready(function(){
    $("#header, .info").ripples({
        dropRadius: 25,
        perturbance: 0.6,
    });
    
    /* popup window gallery */
    $('.parent-container').magnificPopup({
        delegate: 'a', // child items selector, by clicking on it popup will open
        type: 'image',
        gallery:{
            enabled:true
        }
        // other options
    });
    /* toggler button bars change */
    $('.navbar-toggler').click(function(){
        $('.navbar-toggler').toggleClass('change');
    });
    /* sticky navbar */
    $(window).scroll(function(){
        let position = $(this).scrollTop();
        if(position>=718){
            $('.navbar').addClass('navbar-background');
            $('.navbar').addClass('fixed-top');
            $('#back-to-top').addClass('scrollTop');
        }
        else{
            $('.navbar').removeClass('navbar-background');
            $('.navbar').removeClass('fixed-top');
            $('#back-to-top').removeClass('scrollTop');
        }
    });
    /* smooth scroll */
    $('.nav-item a, .header-link, #back-to-top').click(function(event){
        event.preventDefault();
        let target = $(this).attr('href');
        $('html, body').stop().animate({
            scrollTop: $(this.hash).offset().top - 25
        }, 3000);
    });
    
});