var gulp = require('gulp');
var browserSync = require('browser-sync');
var sass = require('gulp-sass');

//Compile sass
gulp.task('sass', function(){
    return gulp.src("src/scss/*.scss")
    .pipe(sass())
    .pipe(gulp.dest("src/css"))
    .pipe(browserSync.stream());
});
//Static server + scss/html files spy
gulp.task('serve', gulp.series('sass', function(){
    browserSync.init({
        server: "./src"
    });
    gulp.watch("src/scss/*.scss", gulp.series('sass'));
    gulp.watch("src/*.html").on('change', browserSync.reload);
}));
//Default task
gulp.task('default', gulp.series('serve'));