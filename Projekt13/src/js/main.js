window.addEventListener('load', () => document.querySelector('.preloader').classList.add('hidePreloader'))

const createWatch = (() => {
    const watches = [];
    
    //watch class
    class Watch{
        constructor(make,country,img,special,model,price,type,movement,strap){
            this.make = make;
            this.country = country;
            this.img = img;
            this.special = special;
            this.model = model;
            this.price = price;
            this.type = type;
            this.movement = movement;
            this.strap = strap;
        }
    }
    
    //watch creation function
    function makeWatch(make,country,img = 'img/quartz1.jpeg',special = true,model = 'new model',price = 1000,type = 'watch',movement = 'quartz',strap = 'fabric'){
        const watch = new Watch(make,country,img,special,model,price,type,movement,strap);
        watches.push(watch);
    }
    //produce watches
    function produceCars(){
        makeWatch('rolex','automatic','img/auto4.jpeg');
        makeWatch('omega','automatic','img/auto5.jpeg',true);
        makeWatch('seiko','automatic','img/quartz4.jpeg',true);
        makeWatch('orient','automatic','img/quartz5.jpeg',true);
        makeWatch('patek','automatic',undefined,true);
        makeWatch('seiko','automatic',undefined,false);
        makeWatch('seiko','quartz',undefined,false);
        makeWatch('seiko','quartz',undefined,false);
        makeWatch('seiko','quartz',undefined,false);
        makeWatch('seiko','quartz',undefined,false);
        makeWatch('seiko','quartz',undefined,false);
    }
    produceCars();
    console.log(watches);
    //special watches
    const specialWatches = watches.filter(watch => watch.special===true);
    console.log(specialWatches);
    
    return{
        watches,
        specialWatches
    }
    
})();

const displaySpecialWatches = ((createWatch) => {
    const specialWatches = createWatch.specialWatches;
    let info = document.querySelector('.featured-info');
    document.addEventListener('DOMContentLoaded',()=>{
        info.innerHTML = '';
        let data = '';
        specialWatches.forEach(item =>{
            data += `<div class="featured-item my-3 d-flex p-2 text-capitalize align-items-baseline flex-wrap">
                        <span data-img="${item.img}" class="featured-icon mr-2">
                            <i class="fas fa-clock"></i>
                        </span>
                        <h5 class="font-weight-bold mx-1">${item.make}</h5>
                        <h5 class="mx-1">${item.model}</h5>
                    </div>`
        })
        info.innerHTML = data;
    })
    info.addEventListener('click', (event)=>{
        if(event.target.parentElement.classList.contains('featured-icon')){
           const img = event.target.parentElement.dataset.img;
           document.querySelector('.featured-photo').src = img;
        }
    })
})(createWatch);