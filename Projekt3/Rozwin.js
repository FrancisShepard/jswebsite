$(document).ready(function(){
	
	$("#prezesA").click(function(){
		$("#prezesOpis").show(1000);
		$("#prezesOpisB").hide(500);
	});
	
	$("#prezesB").click(function(){
		$("#prezesOpisB").show(1000);
		$("#prezesOpis").hide(500);
	});
	
	$(".ukrycie").click(function(){
		$("#prezesOpis").hide(1000);
		$("#prezesOpisB").hide(1000);
	});
	
	$(".next").on("click", function(){
		var currentImg = $(".active");
		var nextImg = currentImg.next();
		
		if(nextImg.length){
			currentImg.fadeOut();
			currentImg.removeClass("active").css("z-index", -10);
			nextImg.fadeIn(2000);
			nextImg.addClass("active").css("z-index", 10);
		}
	});
	
	$(".prev").on("click", function(){
		var currentImg = $(".active");
		var prevImg = currentImg.prev();
		
		if(prevImg.length){
			currentImg.fadeOut();
			currentImg.removeClass("active").css("z-index", -10);
			prevImg.fadeIn(2000);
			prevImg.addClass("active").css("z-index", 10);
		}
	});
	
	$("#btn").click(function(event){
		var email = $(".email").val();
		var sub = $(".subject").val();
		var mes = $(".message").val();
		
		if(email.length > 5 && email.includes("@") && email.includes(".")){
			console.log("Adres Email poprawny");
		}
		else{
			$(".status").text("Niepoprawny adres Email!");
			event.preventDefault();
		}
		
		if(sub.length < 5){
			$(".status").text("Tytuł wiadomości musi zawierać co najmniej 5 znaków!");
			event.preventDefault();
		}
		if(mes.length < 20){
			$(".status").text("Wiadomość musi zawierać co najmniej 20 znaków!");
			event.preventDefault();
		}
	});
	
	$(".email").focus(function(){
		$(this).val("").css("background-color", "#eef2b5");
	});
	$(".email").blur(function(){
		$(this).css("background-color", "white");
	});
	
	$(".subject").focus(function(){
		$(this).val("").css("background-color", "#eef2b5");
	});
	$(".subject").blur(function(){
		$(this).css("background-color", "white");
	});
	
	$(".message").focus(function(){
		$(this).val("").css("background-color", "#eef2b5");
	});
	$(".message").blur(function(){
		$(this).css("background-color", "white");
	});
	
});